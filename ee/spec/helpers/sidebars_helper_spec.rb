# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::SidebarsHelper, feature_category: :navigation do
  include Devise::Test::ControllerHelpers

  describe '#super_sidebar_nav_panel' do
    it 'returns Security Panel for security nav' do
      expect(helper.super_sidebar_nav_panel(nav: 'security')).to be_a(Sidebars::Security::Panel)
    end
  end

  describe '#super_sidebar_context' do
    let_it_be(:user) { build(:user) }
    let_it_be(:panel) { {} }

    before do
      allow(helper).to receive(:current_user) { user }
      allow(user.namespace).to receive(:actual_plan_name).and_return(::Plan::ULTIMATE)
      allow(helper).to receive(:current_user_menu?).and_return(true)
      allow(helper).to receive(:can?).and_return(true)
      allow(helper).to receive(:show_buy_pipeline_with_subtext?).and_return(true)
      allow(panel).to receive(:super_sidebar_menu_items).and_return(nil)
      allow(panel).to receive(:super_sidebar_context_header).and_return(nil)
      allow(user).to receive(:assigned_open_issues_count).and_return(1)
      allow(user).to receive(:assigned_open_merge_requests_count).and_return(4)
      allow(user).to receive(:review_requested_open_merge_requests_count).and_return(0)
      allow(user).to receive(:todos_pending_count).and_return(3)
      allow(user).to receive(:total_merge_requests_count).and_return(4)
    end

    shared_examples 'pipeline minutes attributes' do
      it 'returns sidebar values from user', :use_clean_rails_memory_store_caching do
        expect(subject).to have_key(:pipeline_minutes)
        expect(subject[:pipeline_minutes]).to include({
          show_buy_pipeline_minutes: true,
          show_notification_dot: false,
          show_with_subtext: true,
          tracking_attrs: {
            'track-action': 'click_buy_ci_minutes',
            'track-label': ::Plan::DEFAULT,
            'track-property': 'user_dropdown'
          },
          notification_dot_attrs: {
            'data-track-action': 'render',
            'data-track-label': 'show_buy_ci_minutes_notification',
            'data-track-property': ::Plan::ULTIMATE
          },
          callout_attrs: {
            feature_id: ::Ci::RunnersHelper::BUY_PIPELINE_MINUTES_NOTIFICATION_DOT,
            dismiss_endpoint: '/-/users/callouts'
          }
        })
      end
    end

    context 'when in project scope' do
      before do
        allow(helper).to receive(:show_buy_pipeline_minutes?).and_return(true)
      end

      let_it_be(:project) { build(:project) }
      let_it_be(:group) { nil }

      let(:subject) { helper.super_sidebar_context(user, group: group, project: project, panel: panel) }

      include_examples 'pipeline minutes attributes'

      it 'returns correct usage quotes path', :use_clean_rails_memory_store_caching do
        expect(subject[:pipeline_minutes]).to include({
          buy_pipeline_minutes_path: "/-/profile/usage_quotas"
        })
      end
    end

    context 'when in group scope' do
      before do
        allow(helper).to receive(:show_buy_pipeline_minutes?).and_return(true)
      end

      let_it_be(:group) { build(:group) }
      let_it_be(:project) { nil }

      let(:subject) { helper.super_sidebar_context(user, group: group, project: project, panel: panel) }

      include_examples 'pipeline minutes attributes'

      it 'returns correct usage quotes path', :use_clean_rails_memory_store_caching do
        expect(subject[:pipeline_minutes]).to include({
          buy_pipeline_minutes_path: "/groups/#{group.path}/-/usage_quotas"
        })
      end
    end

    context 'when neither in a group nor in a project scope' do
      before do
        allow(helper).to receive(:show_buy_pipeline_minutes?).and_return(false)
      end

      let_it_be(:project) { nil }
      let_it_be(:group) { nil }

      let(:subject) { helper.super_sidebar_context(user, group: group, project: project, panel: panel) }

      it 'does not have pipeline minutes attributes' do
        expect(subject).not_to have_key('pipeline_minutes')
      end
    end
  end
end
